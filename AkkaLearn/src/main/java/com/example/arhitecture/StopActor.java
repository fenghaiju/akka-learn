/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/7/11/15:32
 * 项目名称: AkkaLearn
 * 文件名称: StopActor.java
 * 文件描述: @Description: ActorLifeCycle
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.example.arhitecture;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

/**
 * 包名称：com.example
 * 类名称：StopActor
 * 类描述：ActorLifeCycle
 * 创建人：@author fengxin
 * 创建时间：2019/7/11/15:32
 */




public class StopActor {
    public static void main(String[] args){
        ActorSystem system = ActorSystem.create("testSystem");
        ActorRef first = system.actorOf(StartStopActor1.props(), "first");
        first.tell("stop", ActorRef.noSender());
    }
}

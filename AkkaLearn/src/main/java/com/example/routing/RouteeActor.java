/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/7/27/14:07
 * 项目名称: AkkaLearn
 * 文件名称: RouteeActor.java
 * 文件描述: @Description: toutee
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.example.routing;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.FromConfig;

/**
 * 包名称：com.example.routing
 * 类名称：RouteeActor
 * 类描述：toutee
 * 创建人：@author fengxin
 * 创建时间：2019/7/27/14:07
 */


public class RouteeActor  extends AbstractActor {
    static Props props() {
        return Props.create(RouteeActor.class, RouteeActor::new);
    }

    @Override
    public void preStart() {
        System.out.println("supervised actor started");
    }

    @Override
    public void postStop() {
        System.out.println("supervised actor stopped");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .matchEquals(
                        "hello",
                        f -> {
                            System.out.println("i:routee");
                        })
                .build();
    }


    public static void main(String[] args){
        ActorSystem actorSystem = ActorSystem.create("routertest");
        //ActorRef router = actorSystem.actorOf(FromConfig.getInstance().)
    }
}
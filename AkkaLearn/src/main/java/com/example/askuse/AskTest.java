/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/7/27/16:03
 * 项目名称: AkkaLearn
 * 文件名称: AskTest.java
 * 文件描述: @Description: asktest
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.example.askuse;

import akka.actor.*;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.example.routing.RouteeActor;
import lombok.SneakyThrows;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;

/**
 * 包名称：com.example.askuse
 * 类名称：AskTest
 * 类描述：asktest
 * 创建人：@author fengxin
 * 创建时间：2019/7/27/16:03
 */


public class AskTest  extends AbstractActor {
    static Props props() {
        return Props.create(AskTest.class, AskTest::new);
    }

    @Override
    public void preStart() {
        System.out.println("actor started");
    }

    @Override
    public void postStop() {
        System.out.println("actor stopped");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .matchEquals(
                        "hello",
                        f -> getSender().tell("nice", getSelf()))
                .matchEquals("free",f -> getSender().tell("pleasure",getSelf()))
                //.matchEquals("finished",f -> getSender().tell("relax",getSelf()))
                .matchEquals("finished",f -> System.out.println(f))
                //.match(PoisonPill.class,f -> System.out.println("killed"))

                .build();
    }

    @SneakyThrows
    public static void main(String[] args) {
        ActorSystem actorSystem = ActorSystem.create("test");
        ActorRef actocRef = actorSystem.actorOf(AskTest.props(), "askTestActor");
        ActorRef actocRef2 = actorSystem.actorOf(AskTest.props(), "askTestActor2");
        ActorRef actocRef3 = actorSystem.actorOf(AskTest.props(), "askTestActor3");

        String msg1 = "hello";
        String msg2 = "free";
        Duration duration = Duration.ofMillis(1000);

        //Timeout timeout = Timeout.create(Duration.ofSeconds(5));
        //Future<Object> future = Patterns.ask(actocRef, msg1, timeout);
        //String result = (String) Await.result(future, timeout.duration());




        CompletableFuture<Object> future1 =
                ask(actocRef, msg1, duration).toCompletableFuture();
        CompletableFuture<Object> future2 =
                ask(actocRef, msg2, duration).toCompletableFuture();



        CompletableFuture<String> transformed =
                CompletableFuture.allOf(future1, future2)
                        .thenApply(
                                v -> {
                                    String x = (String) future1.join();
                                    String s = (String) future2.join();
                                    return "finished";
                                });


        pipe(transformed, actorSystem.dispatcher()).to(actocRef3);

                //future1.get();
        //future2.get();

        //actocRef.tell(PoisonPill.getInstance(),ActorRef.noSender());
        //actocRef2.tell(PoisonPill.getInstance(),ActorRef.noSender());
        //actocRef3.tell(PoisonPill.getInstance(),ActorRef.noSender());

        //actorSystem.terminate();

    }

}

/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/7/11/15:59
 * 项目名称: AkkaLearn
 * 文件名称: IotSuperviser.java
 * 文件描述: @Description: IotSupervisor
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.example.supervisor;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;

/**
 * 包名称：com.example.createfirstactor
 * 类名称：IotSuperviser
 * 类描述：IotSupervisor
 * 创建人：@author fengxin
 * 创建时间：2019/7/11/15:59
 */

public class IotSupervisor extends AbstractActor {
        private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

        public static Props props() {
            return Props.create(IotSupervisor.class, IotSupervisor::new);
        }

        @Override
        public void preStart() {
            log.info("IoT Application started");
        }

        @Override
        public void postStop() {
            log.info("IoT Application stopped");
        }

        // No need to handle any messages
        @Override
        public Receive createReceive() {
            return receiveBuilder().build();
        }
    }